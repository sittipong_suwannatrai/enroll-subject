//
//  User.h
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (strong, nonatomic) NSString *type;
@property (readonly) NSString *username;
@property (readonly) NSString *password;

- (id)initWithUsername:(NSString *)username password:(NSString *)password;
- (BOOL)isAuthenticationWithUsername:(NSString *)username password:(NSString *)password;
- (void)setUserTypeToSpecialStudent;
- (NSUInteger)getCreditCostRate;

@end
