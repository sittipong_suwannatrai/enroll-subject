//
//  Subject.m
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "Subject.h"

@implementation Subject

- (id)initWithTitle:(NSString *)title credit:(NSUInteger)credit midtermExamDate:(NSDate *)midtermExamDate finalExamDate:(NSDate *)finalExamDate {
    self = [super init];
    if (self) {
        _title = title;
        _credit = credit;
        _midtermExamDate = midtermExamDate;
        _finalExamDate = finalExamDate;
    }
    return self;
}

@end
