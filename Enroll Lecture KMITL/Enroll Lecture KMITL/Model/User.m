//
//  User.m
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "User.h"

NSString *kTypeNormalStudent = @"Normal Student";
NSString *kTypeSpecialStudent = @"Special Student";

@implementation User

- (id)initWithUsername:(NSString *)username password:(NSString *)password {
    self = [super init];
    if (self) {
        _username = username;
        _password = password;
        self.type = kTypeNormalStudent;
    }
    return self;
}

- (BOOL)isAuthenticationWithUsername:(NSString *)username password:(NSString *)password {
    return [self.username isEqual:username] && [self.password isEqual:password];
}

- (void)setUserTypeToSpecialStudent {
    self.type = kTypeSpecialStudent;
}

- (NSUInteger)getCreditCostRate {
    if ([self.type isEqualToString:kTypeNormalStudent]) {
        return 300;
    } else if ([self.type isEqualToString:kTypeSpecialStudent]) {
        return 700;
    }
    return 0;
}

@end
