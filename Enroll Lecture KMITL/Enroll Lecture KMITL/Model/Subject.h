//
//  Subject.h
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subject : NSObject

@property (readonly) NSString *title;
@property (readonly) NSUInteger credit;
@property (readonly) NSDate *midtermExamDate;
@property (readonly) NSDate *finalExamDate;

- (id)initWithTitle:(NSString *)title credit:(NSUInteger)credit midtermExamDate:(NSDate *)midtermExamDate finalExamDate:(NSDate *)finalExamDate;

@end
