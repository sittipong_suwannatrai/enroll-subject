//
//  SubjectCatalog.m
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "SubjectCatalog.h"
#import "Subject.h"

@implementation SubjectCatalog {
    NSDictionary *subjectCatalogDictionary;
    NSArray *subjectTypeArray;
}

- (void)loadData:(NSArray *)subjectCollection withSubjectTypeArray:(NSArray *)subjectTypes {
    NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
    NSMutableArray *tempSubjectType = [[NSMutableArray alloc] init];
    for (NSUInteger i=0; i < [subjectCollection count]; i++) {
        NSArray *subjectList = subjectCollection[i];
        [temp setObject:subjectList forKey:subjectTypes[i]];
        [tempSubjectType addObject:subjectTypes[i]];
    }
    subjectCatalogDictionary = [NSDictionary dictionaryWithDictionary:temp];
    subjectTypeArray = [NSArray arrayWithArray:tempSubjectType];
}

- (id)initWithArray:(NSArray *)subjectArray withSubjectTypeArray:(NSArray *)subjectTypes {
    self = [super init];
    if (self) {
        [self loadData:subjectArray withSubjectTypeArray:subjectTypes];
    }
    return self;
}

- (Subject *)getSubjectByType:(NSString *)subjectType atIndex:(NSUInteger)index {
    return [subjectCatalogDictionary objectForKey:subjectType][index];
}

- (Subject *)getSubjectByTypeNumber:(NSUInteger)subjectTypeNumber atIndex:(NSUInteger)index {
    return [self getSubjectByType:subjectTypeArray[subjectTypeNumber] atIndex:index];
}

- (NSString *)getSubjectTypeByTypeNumber:(NSUInteger)subjectTypeNumber {
    return subjectTypeArray[subjectTypeNumber];
}

- (NSUInteger)countSubjectList {
    return [subjectCatalogDictionary count];
}

- (NSUInteger)countBySubjectType:(NSString *)subjectType {
    return [[subjectCatalogDictionary objectForKey:subjectType] count];
}

- (NSUInteger)countBySubjectTypeNumber:(NSUInteger)subjectTypeNumber {
    return [self countBySubjectType:subjectTypeArray[subjectTypeNumber]];
}

@end
