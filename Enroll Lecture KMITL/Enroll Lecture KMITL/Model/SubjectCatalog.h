//
//  SubjectCatalog.h
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Subject;

@interface SubjectCatalog : NSObject

- (id)initWithArray:(NSArray *)subjectCollection withSubjectTypeArray:(NSArray *)subjectTypeArray;
- (Subject *)getSubjectByType:(NSString *)subjectType atIndex:(NSUInteger)index;
- (Subject *)getSubjectByTypeNumber:(NSUInteger)subjectTypeNumber atIndex:(NSUInteger)index;
- (NSString *)getSubjectTypeByTypeNumber:(NSUInteger)subjectTypeNumber;
- (NSUInteger)countSubjectList;
- (NSUInteger)countBySubjectType:(NSString *)subjectType;
- (NSUInteger)countBySubjectTypeNumber:(NSUInteger)subjectTypeNumber;


@end
