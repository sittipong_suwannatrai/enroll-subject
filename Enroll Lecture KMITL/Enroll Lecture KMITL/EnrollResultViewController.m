//
//  EnrollResultViewController.m
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "EnrollResultViewController.h"
#import "User.h"
#import "Subject.h"

@interface EnrollResultViewController ()
@property (weak, nonatomic) IBOutlet UILabel *totalCostLabel;

@end

@implementation EnrollResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSUInteger)calculateTotalCost {
    NSUInteger totalCredit = 0;
    for (Subject *subject in self.selectedSubject) {
        totalCredit += subject.credit;
    }
    return [self.user getCreditCostRate] * totalCredit;
}

- (void)updateLabel {
    NSUInteger totalCost = [self calculateTotalCost];
    self.totalCostLabel.text = [NSString stringWithFormat:@"Total Cost: %lu Baht", totalCost];
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
