//
//  DummyDatabase.m
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "DummyDatabase.h"
#import "User.h"
#import "Subject.h"
#import "SubjectCatalog.h"

@implementation DummyDatabase {
    NSArray *userList;
}

- (void)setInitializationData {
    // Setup dummy users.
    User *user1 = [[User alloc] initWithUsername:@"Sittipong" password:@"56070137"];
    User *user2 = [[User alloc] initWithUsername:@"Attachai" password:@"56070149"];
    
    [user2 setUserTypeToSpecialStudent];
    
    userList = @[user1, user2];
    
    // Setup dummy subjects.
    NSArray *basicSubjectList = @[
                    [[Subject alloc] initWithTitle:@"Computer Programming" credit:3 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]],
                    [[Subject alloc] initWithTitle:@"English" credit:3 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]],
                    [[Subject alloc] initWithTitle:@"Software Development" credit:3 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]]
                    ];
    NSArray *freeSubjectList = @[
                    [[Subject alloc] initWithTitle:@"Human and Tourism" credit:3 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]],
                    [[Subject alloc] initWithTitle:@"Business Management" credit:3 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]],
                    [[Subject alloc] initWithTitle:@"PE" credit:2 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]],
                    [[Subject alloc] initWithTitle:@"Healthy" credit:1 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]]
                    ];
    NSArray *majorSubjectList = @[
                    [[Subject alloc] initWithTitle:@"Internet of Thing" credit:3 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]],
                    [[Subject alloc] initWithTitle:@"iOS Development" credit:5 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]],
                    [[Subject alloc] initWithTitle:@"Lean Startup" credit:2 midtermExamDate:[[NSDate alloc] init] finalExamDate:[[NSDate alloc] init]]
                    ];
    _subjectCatalog = [[SubjectCatalog alloc] initWithArray:@[basicSubjectList, freeSubjectList, majorSubjectList] withSubjectTypeArray:@[@"Basic", @"Free", @"Major"]];
}

- (id)init {
    self = [super init];
    if (self) {
        [self setInitializationData];
    }
    return self;
}

- (User *)authenticationWithUsername:(NSString *)username password:(NSString *)password {
    for (User *user in userList) {
        if ([user isAuthenticationWithUsername:username password:password]) {
            return user;
        }
    }
    return nil;
}

@end
