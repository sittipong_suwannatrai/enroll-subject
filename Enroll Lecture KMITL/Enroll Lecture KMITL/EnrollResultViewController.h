//
//  EnrollResultViewController.h
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;

@interface EnrollResultViewController : UIViewController

@property (strong, nonatomic) NSArray *selectedSubject;
@property (strong, nonatomic) User *user;

@end
