//
//  SelecteEnrollSubjectTableViewController.h
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;

@interface SelecteEnrollSubjectTableViewController : UITableViewController

@property (strong, nonatomic) User *user;

@end
