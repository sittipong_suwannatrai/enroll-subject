//
//  DummyDatabase.h
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;
@class SubjectCatalog;

@interface DummyDatabase : NSObject

@property (readonly) SubjectCatalog *subjectCatalog;

- (User *)authenticationWithUsername:(NSString *)username password:(NSString *)password;

@end
