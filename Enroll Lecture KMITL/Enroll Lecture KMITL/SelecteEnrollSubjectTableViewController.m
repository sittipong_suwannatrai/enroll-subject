//
//  SelecteEnrollSubjectTableViewController.m
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "SelecteEnrollSubjectTableViewController.h"
#import "EnrollResultViewController.h"
#import "DummyDatabase.h"
#import "User.h"
#import "SubjectCatalog.h"
#import "Subject.h"

@interface SelecteEnrollSubjectTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *welcomeUserLabel;

@end

@implementation SelecteEnrollSubjectTableViewController {
    NSUInteger credit;
    DummyDatabase *dummyDatabase;
    SubjectCatalog *subjectCatalog;
    NSMutableArray *selectedSubject;
}

- (void)checkLogin {
    if (self.user == nil) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self checkLogin];
}

- (void)setupIntializationState {
    credit = 22;
    selectedSubject = [[NSMutableArray alloc] init];
    
    dummyDatabase = [[DummyDatabase alloc] init];
    subjectCatalog = dummyDatabase.subjectCatalog;
    [self updateCreditStatusLabel];
}

- (void)viewWillAppear:(BOOL)animated {
    [self setupIntializationState];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)cancelButtonTapped:(id)sender {
    self.user = nil;
    [self checkLogin];
}

- (IBAction)nextButtonTapped:(id)sender {
    [self performSegueWithIdentifier:@"pushEnrollResult" sender:self];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [subjectCatalog countSubjectList];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [subjectCatalog countBySubjectTypeNumber:section];
}

- (void)setupCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Subject *subject = [subjectCatalog getSubjectByTypeNumber:indexPath.section atIndex:indexPath.row];
    cell.textLabel.text = subject.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu", subject.credit];
    if ([selectedSubject containsObject:indexPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subjectCell" forIndexPath:indexPath];
    
    [self setupCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [subjectCatalog getSubjectTypeByTypeNumber:section];
}

- (void)updateCreditStatusLabel {
    self.welcomeUserLabel.text = [NSString stringWithFormat:@"   %@, credit: %lu", self.user.username, credit];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Subject *subject = [subjectCatalog getSubjectByTypeNumber:indexPath.section atIndex:indexPath.row];
    if ([selectedSubject containsObject:indexPath]) {
        [selectedSubject removeObject:indexPath];
        credit += subject.credit;
    } else if (credit >= subject.credit) {
        [selectedSubject addObject:indexPath];
        credit -= subject.credit;
    }
    [self updateCreditStatusLabel];
    [tableView reloadData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqual:@"pushEnrollResult"]) {
        EnrollResultViewController *enrollResultViewController = [segue destinationViewController];
        NSMutableArray *selectedSubjectObject = [[NSMutableArray alloc] init];
        for (NSIndexPath *indexPath in selectedSubject) {
            [selectedSubjectObject addObject:[subjectCatalog getSubjectByTypeNumber:indexPath.section atIndex:indexPath.row]];
        }
        enrollResultViewController.selectedSubject = [NSArray arrayWithArray:selectedSubjectObject];
        enrollResultViewController.user = self.user;
    }
}


@end
