//
//  ViewController.m
//  Enroll Lecture KMITL
//
//  Created by STUDENT on 9/30/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "LoginViewController.h"
#import "SelecteEnrollSubjectTableViewController.h"
#import "DummyDatabase.h"
#import "User.h"

@interface LoginViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorUsernameLabel;

@end

@implementation LoginViewController {
    User *user;
    DummyDatabase *dummyDatabase;
}

- (void)clearWarningMessage {
    self.errorUsernameLabel.textColor = [UIColor clearColor];
}

- (void)resetTextField {
    self.usernameTextField.text = @"";
    self.passwordTextField.text = @"";
}

- (void)setInitializeState {
    [self clearWarningMessage];
    [self resetTextField];
    
    dummyDatabase = [[DummyDatabase alloc] init];
    user = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [self setInitializeState];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (User *)AuthenticationUser {
    return [dummyDatabase authenticationWithUsername:self.usernameTextField.text
                                            password:self.passwordTextField.text];
}

- (void)checkLogin {
    [self clearWarningMessage];

    user = [self AuthenticationUser];
    if (user != nil) {
        NSLog(@"%@", user.username);
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter postNotificationName:@"Authentication" object:user];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        // Warning Message.
        self.errorUsernameLabel.textColor = [[UIColor alloc] initWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
    }
}

- (IBAction)loginButtonTapped:(id)sender {
    [self checkLogin];
    [self performSegueWithIdentifier:@"goMainPage" sender:self];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self checkLogin];
    return YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqual:@"goMainPage"]) {
        SelecteEnrollSubjectTableViewController *selectedEnrollSubjectTableViewController = [[segue destinationViewController] topViewController];
        selectedEnrollSubjectTableViewController.user = user;
    }
}

@end
